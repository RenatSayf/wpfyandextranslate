﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.ComponentModel;
using System.Windows.Input;
using WpfYandexTranslate.Model;

namespace WpfYandexTranslate.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private YandexTranslate _yandexTranslate;
        private YandexAPITranslate _yandexAPITranslate;
        //-----------------------------------------------------------------------------------------
        public MainViewModel()
        {
            _yandexTranslate = new YandexTranslate();
            _yandexAPITranslate = new YandexAPITranslate();
        }
        //-----------------------------------------------------------------------------------------
        public event PropertyChangedEventHandler PropertyChanged;
        //-----------------------------------------------------------------------------------------
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox1;
        public string TextBox1
        {
            get { return _textBox1; }
            set
            {
                if (value != _textBox1)
                {
                    _textBox1 = value;
                    NotifyPropertyChanged("TextBox1");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox2;
        public string TextBox2
        {
            get { return _textBox2; }
            set
            {
                if (value != _textBox2)
                {
                    _textBox2 = value;
                    NotifyPropertyChanged("TextBox2");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox3;
        public string TextBox3
        {
            get { return _textBox3; }
            set
            {
                if (value != _textBox3)
                {
                    _textBox3 = value;
                    NotifyPropertyChanged("TextBox3");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox4;
        public string TextBox4
        {
            get { return _textBox4; }
            set
            {
                if (value != _textBox4)
                {
                    _textBox4 = value;
                    NotifyPropertyChanged("TextBox4");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private string _textBox5;
        public string TextBox5
        {
            get { return _textBox5; }
            set
            {
                if (value != _textBox5)
                {
                    _textBox5 = value;
                    NotifyPropertyChanged("TextBox5");
                }
            }
        }
        //-----------------------------------------------------------------------------------------
        private ICommand _btnClick;
        public ICommand BtnClick
        {
            get
            {
                return _btnClick ?? (_btnClick = new RelayCommand(async (arg) =>
                {
                    TextBox2 = ""; TextBox3 = ""; TextBox4 = ""; TextBox5 = "";
                    var res = await _yandexTranslate.GetTranslateFromYandex(TextBox1);
                    if (res.Count == 1)
                    {
                        TextBox3 = res[0];
                    }
                    if (res.Count == 2)
                    {
                        TextBox3 = res[0];
                        TextBox4 = res[1];
                    }
                    if (res.Count == 3)
                    {
                        TextBox3 = res[0];
                        TextBox4 = res[1];
                        TextBox5 = res[2];
                    }

                    var res2 = await _yandexAPITranslate.GetTranslateFromYandex(_textBox1);
                    if (res2.Count > 0)
                    {
                        TextBox2 = res2[0];
                    }
                }));
            }
        }


    }
}
