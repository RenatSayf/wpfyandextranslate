﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Web;

namespace WpfYandexTranslate.Model
{
    public class YandexAPITranslate
    {
        public async Task<List<string>> GetTranslateFromYandex(string text)
        {
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            text = text.ToLower();
            
            string lang = "en-ru";
            if (text.Any(wordByte => wordByte > 127))
            {
                lang = "ru-en";
            }
            
            string keyApi = "trnsl.1.1.20160128T151725Z.d4bbd1b06137bfde.2e1323688363a820f730629556d68f2b9f0a1a19";
            string format = "plain";
            text = WebUtility.UrlEncode(text);
            string url = "https://translate.yandex.net/api/v1.5/tr/translate?key=" + keyApi + "&text=" + text + "&lang=" + lang + "&[format=" + format + "]&[options=1]";
            
            Uri uri = new Uri(url);
            
            string s = "";
            try
            {
                var sa = await client.UploadStringTaskAsync(url, text);
                Stream data = await client.OpenReadTaskAsync(uri);
                StreamReader reader = new StreamReader(data);
                s = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
            }
            List<string> list = new List<string>();

            XmlDocument docXml = new XmlDocument();
            docXml.LoadXml(s);
            foreach (XmlNode node in docXml.SelectNodes("Translation"))
            {
                foreach (XmlNode item in node.ChildNodes)
                {
                    list.Add(item.InnerText);
                }
            }
            return list;
        }

        



    }
}
