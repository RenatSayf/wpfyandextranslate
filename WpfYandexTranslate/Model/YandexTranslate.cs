﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;

namespace WpfYandexTranslate.Model
{
    public class YandexTranslate
    {
        //-----------------------------------------------------------------------------------------
        public async Task<List<string>> GetTranslateFromYandex(string text)
        {
            WebClient client = new WebClient();
            text = text.ToLower();
            
            string lang = "en-ru";
            if (text.Any(wordByte => wordByte > 127))
            {
                lang = "ru-en";
            }

            text = WebUtility.UrlEncode(text);
            string url = "https://dictionary.yandex.net/dicservice.json/lookup?ui=ru&srv=tr-text&sid=403f73d7.56a74845.3aa8a7cf&text=" + text + "&lang=" + lang + "&flags=23";

            Uri uri = new Uri(url);

            client.Headers.Add("Accept", "*/*");
            client.Headers.Add("Accept-Encoding", "gzip, deflate, sdch");
            client.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            client.Headers.Add("Host", "dictionary.yandex.net");
            client.Headers.Add("Origin", "https://translate.yandex.ru");
            client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36");
            string stringData = "";
            try
            {
                Stream data = await client.OpenReadTaskAsync(uri);
                StreamReader reader = new StreamReader(data);
                stringData = reader.ReadToEnd();

                data.Close();
                reader.Close();
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
            }
            List<string> list = new List<string>();
            RootObject rootJson = new RootObject();
            try
            {
                rootJson = JsonConvert.DeserializeObject<RootObject>(stringData);
                if (text.Any(wordByte => wordByte < 127) && rootJson.def.Count > 0 && rootJson.def[0].tr.Count > 0 && rootJson.def[0].tr[0].syn != null && rootJson.def[0].tr[0].syn.Count > 0)
                {
                    var translate = rootJson.def[0].tr;
                    list.Add(translate[0].text);
                    for (int i = 0; translate != null && i < translate.Count; i++)
                    {
                        for (int j = 0; translate[i].syn != null && j < translate[i].syn.Count; j++)
                        {
                            list.Add(translate[i].syn[j].text.ToString());
                        }
                    }
                }
                if (text.Any(wordByte => wordByte > 127) && rootJson.def.Count > 0 && rootJson.def[0].tr.Count > 0)
                {
                    var translate = rootJson.def[0].tr;
                    list.Add(translate[0].text);
                    for (int i = 0; translate != null && i < translate.Count; i++)
                    {
                        for (int j = 0; translate[i].syn != null && j < translate[i].syn.Count; j++)
                        {
                            list.Add(translate[i].syn[j].text.ToString());
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Debug.WriteLine(err.Message);
            }


            return list;
        }
    }
    //=============================================================================================
    public class Head
    {
        public bool more { get; set; }
    }

    public class Syn
    {
        public string text { get; set; }
        public string pos { get; set; }
        public string asp { get; set; }
    }

    public class Mean
    {
        public string text { get; set; }
    }

    public class Tr2
    {
        public string text { get; set; }
    }

    public class Ex
    {
        public string text { get; set; }
        public List<Tr2> tr { get; set; }
    }

    public class Tr
    {
        public string text { get; set; }
        public string pos { get; set; }
        public string asp { get; set; }
        public List<Syn> syn { get; set; }
        public List<Mean> mean { get; set; }
        public List<Ex> ex { get; set; }
    }

    public class Def
    {
        public string text { get; set; }
        public string pos { get; set; }
        public string ts { get; set; }
        public List<Tr> tr { get; set; }
    }

    public class RootObject
    {
        public Head head { get; set; }
        public List<Def> def { get; set; }
    }

}
